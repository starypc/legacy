This repository holds the legacy [stary.pc.pl](https://stary.pc.pl) website and other old files.

Go to [this repository](https://gitlab.com/starypc/website) if you want to see the files for the current website, because you probably don't want *this*.

The repo is public domain ([The Unlicense](LICENSES/The_Unlicense)), however the website contents are available under [CC BY-SA 4.0](LICENSES/CC_BY-SA_4.0). See the [COPYING](COPYING) file.
