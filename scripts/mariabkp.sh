#!/bin/sh
# mariabkp.sh - mariabackup wrapper script.

# TODO
# - CLI options
# - config
# - more features

baseDir="/mnt/backups/mariadb"
mariaCmd="mariabackup"
#mariaUser="user here or set in shell"
#mariaPassword="password here or set in shell"

auth="--user=$mariaUser --password=$mariaPassword"

if [ ! -e "$baseDir/full" ]; then
    $mariaCmd --backup --target-dir="$baseDir/full" $auth
else
    current=$(ls -d "$baseDir/inc"* 2>/dev/null | tail -n 1 | grep -Po "\d+")
    if [ -z $current ]; then
        srcDir="full"
    else
        srcDir=inc$current
    fi
    incDir=inc$(($current + 1))
    $mariaCmd --backup --target-dir="$baseDir/$incDir" --incremental-basedir="$baseDir/$srcDir" $auth
fi
